﻿using System;

namespace SharpBank
{
    public interface IInterestCalculator
    {
        double CalculateInterest(double amount, DateTime dateOpenAccount);
    }
}
