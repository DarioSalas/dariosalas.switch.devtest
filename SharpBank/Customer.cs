﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpBank
{
    public class Customer
    {
        private readonly String name;
        private List<Account> accounts;

        public Customer(String name)
        {
            this.name = name;
            this.accounts = new List<Account>();
        }

        public String GetName() => name;

        public Customer OpenAccount(Account account)
        {
            accounts.Add(account);
            return this;
        }

        public int GetNumberOfAccounts() => accounts.Count;

        public double TotalInterestEarned() => accounts.Sum(sum => sum.InterestEarned());


        /*******************************
         * This method gets a statement
         *********************************/
        public String GetStatement()
        {
            //JIRA-123 Change by Joe Bloggs 29/7/1988 start
            StringBuilder statement = new StringBuilder();
            statement.Append($"Statement for {name}\n");//reset statement to null here    
            //JIRA-123 Change by Joe Bloggs 29/7/1988 end

            statement.Append(accounts.Aggregate(
                     new StringBuilder(),
                         (current, next) => current.Append(current.Length == 0 ? "" : "").Append("\n" + StatementForAccount(next) + "\n"))
                         .ToString());

            var total = accounts.Sum(s => s.SumTransactions());

            statement.Append("\nTotal In All Accounts " + ToDollars(total));

            return statement.ToString();
        }

        private String StatementForAccount(Account account)
        {
            StringBuilder statement = new StringBuilder();

            //Translate to pretty account type
            switch (account.GetAccountType())
            {
                case AccountType.Checking:
                    statement.Append("Checking Account\n");
                    break;
                case AccountType.Savings:
                    statement.Append("Savings Account\n");
                    break;
                case AccountType.Maxi_Savings:
                    statement.Append("Maxi Savings Account\n");
                    break;
            }

            //Now total up all the transactions

            statement.Append(account.Transactions.Aggregate(
                     new StringBuilder(),
                         (current, next) => current.Append($"  {(next.Amount < 0 ? "withdrawal" : "deposit")} {ToDollars(next.Amount)}\n"))
                         .ToString());

            var total = account.Transactions.Sum(sum => sum.Amount);

            statement.Append($"Total {ToDollars(total)}");

            return statement.ToString();
        }

        private String ToDollars(double amount) => String.Format("${0:N2}", Math.Abs(amount));
    }
}
