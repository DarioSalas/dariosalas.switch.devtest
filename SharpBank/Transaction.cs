﻿using System;

namespace SharpBank
{
    public class Transaction
    {
        public double Amount { get; private set; }

        private readonly DateTime transactionDate;

        public Transaction(double amount)
        {
            this.Amount = amount;
            this.transactionDate = DateProvider.GetInstance().Now();
        }
    }
}
