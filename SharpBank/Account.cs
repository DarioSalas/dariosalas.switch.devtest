﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SharpBank
{
    public class Account
    {

        private readonly AccountType accountType;
        private readonly string idAccount;
        private readonly string owner;
        private IInterestCalculator interestCalculator;

        public Account(AccountType accountType, string owner, DateTime dateCreate)
        {
            this.owner = owner;
            this.idAccount = Guid.NewGuid().ToString();
            this.accountType = accountType;
            this.Transactions = new List<Transaction>();
            this.DateCreate = dateCreate;
        }

        public DateTime LastWithdraw { get; private set; } //Should come from database 
        public DateTime DateCreate { get; }   //Should come from database
        public List<Transaction> Transactions { get; private set; }

        public void Deposit(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }

            Transactions.Add(new Transaction(amount));
        }

        public void Withdraw(double amount, DateTime dateTime)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }

            Transactions.Add(new Transaction(-amount));
            LastWithdraw = dateTime; 
        }

        public double InterestEarned()
        {
            double amount = SumTransactions();

            switch (accountType)
            {
                case AccountType.Savings:
                    interestCalculator = new InterestCalculatorSavings();
                    return interestCalculator.CalculateInterest(amount, this.DateCreate);

                case AccountType.Maxi_Savings:
                    interestCalculator = new InterestCalculatorMaxiSaving(this.LastWithdraw);
                    return  interestCalculator.CalculateInterest(amount, DateCreate);

                default:
                    interestCalculator = new InterestCalculatorGeneral();
                    return interestCalculator.CalculateInterest(amount, this.DateCreate);
            }
        }
        
        public double SumTransactions() => Transactions.Sum(sum => sum.Amount);

        public bool Transfer(Account targetAccount)
        {
            try
            {
                ValidateTransfer(targetAccount);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }

            return true;
        }

        private void ValidateTransfer(Account targetAccount)
        {
            if (this.owner != targetAccount.owner)
            {
                throw new ArgumentException("It is not possible to transfer money between accounts from different owners");
            }

            if (this.idAccount == targetAccount.idAccount)
            {
                throw new ArgumentException("It is not possible to transfer money to the same account");
            }
        }


        public AccountType GetAccountType() => accountType;
    }

    public enum AccountType
    {
        Checking = 0,
        Savings = 1,
        Maxi_Savings = 2
    }
}
