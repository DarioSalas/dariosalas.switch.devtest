﻿using System;

namespace SharpBank
{
    public class InterestCalculatorGeneral : IInterestCalculator
    {
        private const double PERCENTAGEPERYEAR = 5;

        public double CalculateInterest(double amount, DateTime dateOpenAccount)
        {
            var interestPerDay = Math.Round(((double)PERCENTAGEPERYEAR / (double)365), 3);

            var countDayLast = (int)(System.DateTime.Now - dateOpenAccount).TotalDays;

            amount += (amount * (countDayLast * interestPerDay));

            return amount * 0.001;
        }
    }

    public class InterestCalculatorSavings : InterestCalculatorGeneral, IInterestCalculator
    {
        private const double PERCENTAGEPERYEAR = 5;

        public new double CalculateInterest(double amount, DateTime dateOpenAccount)
        {
            var interestPerDay = Math.Round(((double)PERCENTAGEPERYEAR / (double)365), 3);

            var countDayLast = (int)(System.DateTime.Now - dateOpenAccount).TotalDays;

            amount += (amount * (countDayLast * interestPerDay));

            if (amount <= 1000)
                return amount * 0.001;
            else
                return 1 + (amount - 1000) * 0.002;
        }
    }

    public class InterestCalculatorMaxiSaving : InterestCalculatorGeneral, IInterestCalculator
    {
        public readonly DateTime? lastWithdraw; 

        private const double PERCENTAGELESS10DAYSWITHDRAW = 0.1;
        private const double PERCENTAGEMORE10DAYSWITHDRAW = 5;

        public InterestCalculatorMaxiSaving(DateTime? lastWithdraw)
        {
            this.lastWithdraw = lastWithdraw; 
        }
        public new double CalculateInterest(double amount, DateTime dateOpenAccount)
        {
            var shouldApplyMaxiSavingInterest = lastWithdraw == null || (int)(DateTime.Now - lastWithdraw.Value).TotalDays > 10;

            var interestPerYear =  (shouldApplyMaxiSavingInterest ? (amount * (PERCENTAGEMORE10DAYSWITHDRAW / 100)) : (amount * (PERCENTAGELESS10DAYSWITHDRAW / 100)));

            var countDayLast = (int)(System.DateTime.Now - dateOpenAccount).TotalDays;

            amount += interestPerYear / 365 * countDayLast;

            if (amount <= 1000)
                return amount * 0.02;
            if (amount <= 2000)
                return 20 + (amount - 1000) * 0.05;
            return 70 + (amount - 2000) * 0.1;
        }
    }
}
