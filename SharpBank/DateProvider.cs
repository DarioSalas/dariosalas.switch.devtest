﻿using System;

namespace SharpBank
{
    public class DateProvider
    {
        private static DateProvider instance = null;

        private DateProvider(){}

        public static DateProvider GetInstance()
        {
            if (instance == null)
                instance = new DateProvider();
            return instance;
        }

        public DateTime Now() => DateTime.Now;
    }
}
