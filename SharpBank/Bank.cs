﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpBank
{
    public class Bank
    {
        private List<Customer> customers;

        public Bank()
        {
            customers = new List<Customer>();
        }

        public void AddCustomer(Customer customer)
        {
            customers.Add(customer);
        }

        public String CustomerSummary()
        {
            var summary = customers.Aggregate(
                     new StringBuilder(),
                         (current, next) => current.Append(current.Length == 0 ? "" : ",").Append($"\n - {next.GetName().Trim()} ({Format(next.GetNumberOfAccounts(),"account")})"))
                         .ToString();

            return $"Customer Summary{summary}";
        }

        //Make sure correct plural of word is created based on the number passed in:
        //If number passed in is 1 just return the word otherwise add an 's' at the end
        private String Format(int number, String word)
            => $"{number.ToString()} {(number == 1 ? word : word + "s")}";


        public double TotalInterestPaid() => customers.Sum(sum => sum.TotalInterestEarned());

        public String GetFirstCustomer()
        {
            if (!customers.Any())
            {
                Console.WriteLine("Error: there are not customers to get...");
                return "Error";
            }

            return customers.FirstOrDefault().GetName();
        }
    }
}
