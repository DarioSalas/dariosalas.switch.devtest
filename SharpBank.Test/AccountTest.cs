﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank.Test
{
    [TestFixture]
    public class AccountTest
    {
        private static readonly double DOUBLE_DELTA = 1e-15;

        [Test]
        public void InterestEarnedwith_MaxiSavingsAccount_WhenNoWithdrawns()
        {
            var dateCreate = DateTime.Now.AddYears(-1);
            var expectedInterestPerYear = 185;
            var customerName = "Bill";

            var checkingAccount = new Account(AccountType.Maxi_Savings, customerName, dateCreate);
            checkingAccount.Deposit(3000.0);

            var result = checkingAccount.InterestEarned();

            Assert.AreEqual(result, expectedInterestPerYear, DOUBLE_DELTA);
        }

        [Test]
        public void InterestEarnedwith_MaxiSavingsAccount_WithDrawLessThan10Days()
        {
            var dateCreate = DateTime.Now.AddYears(-1);
            var expectedInterestPerYear = 170.3;
            var customerName = "Bill";

            var checkingAccount = new Account(AccountType.Maxi_Savings, customerName, dateCreate);
            checkingAccount.Deposit(3020.0);
            checkingAccount.Withdraw(20, DateTime.Now.AddDays(-7));

            var result = checkingAccount.InterestEarned();

            Assert.AreEqual(result, expectedInterestPerYear, DOUBLE_DELTA);
        }

        [Test]
        public void InterestEarnedwith_MaxiSavingsAccount_WithDrawGreaterThan10Days()
        {
            var dateCreate = DateTime.Now.AddYears(-1);
            var expectedInterestPerYear = 185;
            var customerName = "Bill";

            var checkingAccount = new Account(AccountType.Maxi_Savings, customerName, dateCreate);
            checkingAccount.Deposit(3020.0);
            checkingAccount.Withdraw(20, DateTime.Now.AddDays(-25));

            var result = checkingAccount.InterestEarned();

            Assert.AreEqual(result, expectedInterestPerYear, DOUBLE_DELTA);
        }

        [Test]
        public void InterestEarnedwith_SavingAccount()
        {
            var dateCreate = DateTime.Now.AddYears(-1);
            var expectedInterestPerYear = 35.66;
            var customerName = "Bill";

            var checkingAccount = new Account(AccountType.Savings, customerName, dateCreate);
            checkingAccount.Deposit(3000.0);

            var result = Math.Round(checkingAccount.InterestEarned(), 2);

            Assert.AreEqual(result, expectedInterestPerYear, DOUBLE_DELTA);
        }

        [Test]
        public void InterestEarnedwith_CheckingAccount()
        {
            var dateCreate = DateTime.Now.AddYears(-1);
            var expectedInterestPerYear = 18.33;
            var customerName = "Bill";

            var checkingAccount = new Account(AccountType.Checking, customerName, dateCreate);
            checkingAccount.Deposit(3000.0);

            var result = Math.Round(checkingAccount.InterestEarned(), 2);

            Assert.AreEqual(result, expectedInterestPerYear, DOUBLE_DELTA);
        }
    }
}
