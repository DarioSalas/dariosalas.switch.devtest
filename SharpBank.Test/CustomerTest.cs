﻿using NUnit.Framework;
using System;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {

        [Test]
        public void TestCustomerStatementGeneration()
        {
            var customerName = "Henry";
            var expectedResult = "Statement for Henry\n" +
                    "\n" +
                    "Checking Account\n" +
                    "  deposit $100.00\n" +
                    "Total $100.00\n" +
                    "\n" +
                    "Savings Account\n" +
                    "  deposit $4,000.00\n" +
                    "  withdrawal $200.00\n" +
                    "Total $3,800.00\n" +
                    "\n" +
                    "Total In All Accounts $3,900.00";

            var result = string.Empty;

            var checkingAccount = new Account(AccountType.Checking, customerName, DateTime.Now);
            var savingsAccount = new Account(AccountType.Savings, customerName, DateTime.Now);

            var henry = new Customer(customerName).OpenAccount(checkingAccount).OpenAccount(savingsAccount);

            checkingAccount.Deposit(100.0);
            savingsAccount.Deposit(4000.0);
            savingsAccount.Withdraw(200.0, DateTime.Now);

            result = henry.GetStatement();

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void TestOneAccount()
        {
            var customerName = "Oscar";
            var oscar = new Customer(customerName).OpenAccount(new Account(AccountType.Savings, customerName, DateTime.Now));
            Assert.AreEqual(1, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestTwoAccount()
        {
            var customerName = "Oscar";
            var oscar = new Customer(customerName)
                    .OpenAccount(new Account(AccountType.Savings, customerName, DateTime.Now));
            oscar.OpenAccount(new Account(AccountType.Checking, customerName, DateTime.Now));
            Assert.AreEqual(2, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestThreeAcounts()
        {
            var customerName = "Oscar";
            var oscar = new Customer(customerName)
                    .OpenAccount(new Account(AccountType.Savings, customerName, DateTime.Now));
            oscar.OpenAccount(new Account(AccountType.Checking, customerName, DateTime.Now));
            oscar.OpenAccount(new Account(AccountType.Maxi_Savings, customerName, DateTime.Now));
            Assert.AreEqual(3, oscar.GetNumberOfAccounts());
        }
    }
}
