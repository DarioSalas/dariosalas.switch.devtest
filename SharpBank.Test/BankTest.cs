﻿using NUnit.Framework;
using System;

namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest
    {
        private static readonly double DOUBLE_DELTA = 1e-15;

        [Test]
        public void CustomerSummary()
        {
            var customerName = "John";
            var expectedResult = "Customer Summary\n - John (1 account)";
            var result = string.Empty;
            
            var john = new Customer(customerName);
            john.OpenAccount(new Account(AccountType.Checking, customerName, DateTime.Now));
            
            var bank = new Bank();
            bank.AddCustomer(john);

            result = bank.CustomerSummary();

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void CheckingAccount()
        {
            var customerName = "Bill";
            
            var checkingAccount = new Account(AccountType.Checking, customerName, DateTime.Now);
            var bill = new Customer(customerName).OpenAccount(checkingAccount);

            var bank = new Bank();
            bank.AddCustomer(bill);

            checkingAccount.Deposit(100.0);

            var result = bank.TotalInterestPaid();

            Assert.AreEqual(0.1, result, DOUBLE_DELTA);
        }

        [Test]
        public void SavingsAccount()
        {
            var customerName = "Bill";
            
            var checkingAccount = new Account(AccountType.Savings, customerName, DateTime.Now);
            checkingAccount.Deposit(1500.0);

            var bank = new Bank();
            bank.AddCustomer(new Customer(customerName).OpenAccount(checkingAccount));
            
            var result = bank.TotalInterestPaid();

            Assert.AreEqual(2.0, result, DOUBLE_DELTA);
        }

        [Test]
        public void TransferDifferentAccountSameOwner()
        {
            var customerName = "Oscar";
            var one = new Account(AccountType.Savings, customerName, DateTime.Now);
            var two = new Account(AccountType.Checking, customerName, DateTime.Now);

            Assert.AreEqual(true, one.Transfer(two));
        }

        [Test]
        public void TransferDifferentAccountDiffenteOwner()
        {
            var customerNameOne = "Oscar";
            var customerNameTwo = "Dario";
            var one = new Account(AccountType.Savings, customerNameOne, DateTime.Now);
            var two = new Account(AccountType.Checking, customerNameTwo, DateTime.Now);

            var exception = Assert.Throws<ArgumentException>(() => one.Transfer(two));

            Assert.That(exception.Message, Is.EqualTo("It is not possible to transfer money between accounts from different owners"));

        }

        [Test]
        public void TransferSameAccount()
        {
            var customerName = "Oscar";
            var one = new Account(AccountType.Savings, customerName, DateTime.Now);

            var exception = Assert.Throws<ArgumentException>(() => one.Transfer(one));

            Assert.That(exception.Message, Is.EqualTo("It is not possible to transfer money to the same account"));
        }

        [Test]
        public void MaxiSavingsAccount_WhenNoWithdrawns()
        {
            var dateCreate = DateTime.Now.AddYears(-1);
            var interestPerYear = 370;

            var customerNameOne = "Bill";
            var customerNameTwo = "Dario";
            var bank = new Bank();

            var checkingAccountOne = new Account(AccountType.Maxi_Savings, customerNameOne, dateCreate);
            bank.AddCustomer(new Customer(customerNameOne).OpenAccount(checkingAccountOne));
            checkingAccountOne.Deposit(3000.0);

            var checkingAccountTow = new Account(AccountType.Maxi_Savings, customerNameTwo, dateCreate);
            bank.AddCustomer(new Customer(customerNameTwo).OpenAccount(checkingAccountTow));
            checkingAccountTow.Deposit(3000.0);

            var result = bank.TotalInterestPaid();

            Assert.AreEqual(result, interestPerYear, DOUBLE_DELTA);
        }

        [Test]
        public void MaxiSavingsAccount_WithDrawLessThan10Days()
        {
            var dateCreate = DateTime.Now.AddYears(-1);
            var interestPerYear = 340.6;

            var customerNameOne = "Bill";
            var customerNameTwo = "Dario";
            var bank = new Bank();

            var checkingAccountOne = new Account(AccountType.Maxi_Savings, customerNameOne, dateCreate);
            checkingAccountOne.Deposit(3020.0);
            checkingAccountOne.Withdraw(20, DateTime.Now.AddDays(-7));
            bank.AddCustomer(new Customer(customerNameOne).OpenAccount(checkingAccountOne));

            var checkingAccountTow = new Account(AccountType.Maxi_Savings, customerNameTwo, dateCreate);
            checkingAccountTow.Deposit(3020.0);
            checkingAccountTow.Withdraw(20, DateTime.Now.AddDays(-7));
            bank.AddCustomer(new Customer(customerNameTwo).OpenAccount(checkingAccountTow));

            var result = bank.TotalInterestPaid();

            Assert.AreEqual(result, interestPerYear, DOUBLE_DELTA);
        }

        [Test]
        public void MaxiSavingsAccount_WithDrawGreaterThan10Days()
        {
            var dateCreate = DateTime.Now.AddYears(-1);
            var interestPerYear = 370;

            var customerNameOne = "Bill";
            var customerNameTwo = "Dario";
            var bank = new Bank();

            var checkingAccountOne = new Account(AccountType.Maxi_Savings, customerNameOne, dateCreate);
            checkingAccountOne.Deposit(3020.0);
            checkingAccountOne.Withdraw(20, DateTime.Now.AddDays(-25));
            bank.AddCustomer(new Customer(customerNameOne).OpenAccount(checkingAccountOne));

            var checkingAccountTwo = new Account(AccountType.Maxi_Savings, customerNameTwo, dateCreate);
            checkingAccountTwo.Deposit(3020.0);
            checkingAccountTwo.Withdraw(20, DateTime.Now.AddDays(-25));
            bank.AddCustomer(new Customer(customerNameTwo).OpenAccount(checkingAccountTwo));

            var result = bank.TotalInterestPaid();

            Assert.AreEqual(result, interestPerYear, DOUBLE_DELTA);
        }
    }
}
